Source: iraf-xdimsum
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               iraf-dev,
               iraf-noao-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/debian-astro-team/iraf-xdimsum
Vcs-Git: https://salsa.debian.org/debian-astro-team/iraf-xdimsum.git
Homepage: https://github.com/iraf-community/iraf-xdimsum
Rules-Requires-Root: no

Package: iraf-xdimsum
Architecture: any
Multi-Arch: foreign
Depends: iraf,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: iraf-fitsutil
Enhances: iraf
Description: Deep Infrared Mosaicing Software
 XDIMSUM is a package for creating accurate sky subtracted images from
 sets of dithered observations. While the observations need not be in
 the infrared, the dominance of the variable sky background in
 infrared data requires the dithering and recombination of many short
 carefully sky subtracted exposures to produce deep images.
